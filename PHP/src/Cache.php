<?php 

	namespace App;

	/**
	 * @class Cache
	 * 
	 * @brief Operaciones basicas de cacheo.
	 * 
	 */
	class Cache extends ConectCache
	{
		
		/**
		* Guarda noticia en cache
		*
		* @param int    $id
		* @param string $noticia
		* @param string $titulo
		* @param int    $user_id, identificador unico del usuario
		* @param string hora
		*
		* @return void
		*/
		public static function cacheSave($id, $noticia, $titulo, $user_id, $hora)
		{
			$memcache_obj= self::getInstance();

			$key = "interview_noticias";

			$row = array('id' => $id ,'noticia' => $noticia , 'titulo' => $titulo, 'user_id' => $user_id, 'hora' => $hora );

			$memcache_obj->set($key."_".$id."_".$user_id, $row,0);

		}



		/**
		* Obtiene registro en cache a partir de la llave.
		*
		* @param int  $id
		* @param int  $user_id
		*
		* @return array, el registro completo
		*/
		public static function cacheGet($id, $user_id)
		{
			$memcache_obj = self::getInstance();
		
			$key = "interview_noticias";

			return $memcache_obj->get($key."_".$id."_".$user_id);
		}


		/**
		* Actualiza un registro en cache.
		*
		* @param int    $id
		* @param string $noticia
		* @param string $titulo
		* @param int    $user_id, identificador unico del usuario
		* @param string hora
		*
		* @return void
		*/
		public static function cacheUpdate($id, $noticia, $titulo, $user_id, $hora)
		{
			$memcache_obj = self::getInstance();
	
			$key = "interview_noticias";
		    $row = array('id' => $id ,'noticia' => $noticia , 'titulo' => $titulo, 'user_id' => $user_id, 'hora' => $hora );

			$memcache_obj->replace($key."_".$id."_".$user_id, $row, 0);
			

		}


		/**
		* Borra un registro en cache a partir de su llave.
		*
		* @param int    $id
		* @param int    $user_id, identificador unico del usuario
		*
		* @return void
		*/
		public static function cacheDelete($id,$user_id)
		{
			$memcache_obj = self::getInstance();
		
			$key = "interview_noticias";

			$memcache_obj->delete($key."_".$id."_".$user_id);

		}


		/**
		* Obtiene una lista de las llaves en cache que coincidan con el prefijo.
		*
		* @param string $prefix
		*
		* @return array $allKeys, 
		*/
		public static function getkeys($prefix = false) 
			{	

			    $m = self::getInstance();

			    $allKeys = [];
			    
			  
			    
			    $keys = $m->getAllKeys();
			
			    if ($prefix !== false) {
			    
			        foreach ($keys as $index => $key) {
			    			
			            if (strpos($key,$prefix) !== 0) {
			    
			                
			    
			            } else {
			    			//print_r($key);
			    			array_push($allKeys, $key);
			                //echo "<br>";
			               //$m->delete($key);
			    
			            }
			    
			        }
			    } 
			    return $allKeys;
		}



		/**
		* Lista de los Id de los usuarios con registros en cache
		*
		* @return array $allKeys
		*/
		public static function userKeys()
		{

		    $allKeys = [];

			$keys = self::getkeys("interview_noticias_");

			foreach ($keys as $key => $value) {
				
				$word = explode("_", $value);
				$length = count($word);

				array_push($allKeys, $word[3]);

			}

			return $allKeys;
		}


		/**
		* Lista de los Id de noticias almacenados en cache.
		*
		* @return array $allKeys
		*/
		public static function noticiaId()
		{

		    $allKeys = [];

			$keys = self::getkeys("interview_noticias_");

			foreach ($keys as $key => $value) {
				
				$word = explode("_", $value);
				$length = count($word);

				array_push($allKeys, $word[2]);

			}

			return $allKeys;
		}
	}