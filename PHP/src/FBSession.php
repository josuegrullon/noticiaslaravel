<?php 

	namespace App;

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookSDKException;
	use Facebook\GraphUser;
	use Facebook\FacebookResponse;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;



	/**
	 * @class FBSession
	 * 
	 * @brief Gestiona el request para sesion de facebook.
	 * 
	 */

	class FBSession
	{	
		/**
		 * Id unico de de api.
		 *
		 * @var $id
		 */
		public static $id = '606739579428282';

		/**
		 * Hash secreto de api.
		 *
		 * @var $secret
		 */
		public static $secret = '2ed02b2b8273e14605f495a567fce634';



		/**
		* Envia peticion de sesion.
		*
		* @return FacebookSession
		*/
		public static function fbLogin()
		{
			FacebookSession::setDefaultApplication(FBSession::$id, FBSession::$secret);

   			 $helper = new FacebookRedirectLoginHelper('http://10.0.0.101/josue/Noticias/PHP/pages/link.php');

			return $helper->getLoginUrl();
		}

	}



 ?>