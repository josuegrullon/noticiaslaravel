<?php 

	namespace App;

	use App\ConectDatabase;
	use App\Cache;

	/**
	 * @class Noticia
	 * 
	 * @brief Media las operaciones con la tabla noticia en la base de datos.
	 * 
	 */
	class Noticia 
	{
		
		/**
		* Inserta noticia nueva
		*
		* @param string $noticia
		* @param string $titulo
		* @param int    $user_id, identificador unico del usuario
		* @param string hora
		*
		* @return void
		*/
		public  function insertNoticia($noticia, $titulo, $user_id, $hora)
		{

			try {

	    	    $count = ConectDatabase::getInstance()->exec("INSERT INTO noticias(noticia, titulo, user_id, hora)
			 	VALUES ('$noticia', '$titulo', '$user_id', '$hora')");

		    	    
		    } catch (\Exception $e) {

		    	echo $e->getMessage();
		    }

		   	$id = ConectDatabase::getInstance()->lastInsertId(); 

	    	Cache::cacheSave($id, $noticia, $titulo, $user_id, $hora);
		}
		

		/**
		 * Borra noticia existente a partir de su id
		 *
		 * @param int $noticia_id
		 *
		 * @return void
		 */
		public  function deleteNoticia($noticia_id)
		{
			try {
				
		  		ConectDatabase::getInstance()->exec("DELETE FROM `interview`.`noticias` WHERE `noticias`.`id` = '$noticia_id'") or die('no se pudo  borrar');
			    
			} catch (\Exception $e) {

		    	echo $e->getMessage();
		    }
			$user_id = $_SESSION['user_id'];
			Cache::cacheDelete($noticia_id, $user_id);
		}	



	/**
		*Actualiza noticia existente a parir de su id
		*
		*@param string $noticia
		*@param string $titulo
		*@param int    $id_noticia
		*@param string $hora
		*
		*@return void
	*/
		public function updateNoticia($noticia, $titulo, $id_noticia,  $hora)
		{	
			try {
				
				 ConectDatabase::getInstance()->exec("UPDATE `noticias` SET `noticia` = '$noticia', `titulo` ='$titulo',
			    `hora` = '$hora' WHERE id= '$id_noticia' ") or die('no se pudo actualizar');
			    
			} catch (\Exception $e) {

		    	echo $e->getMessage();
		    }
		    
		    $user_id = $_SESSION['user_id'];

		    Cache::cacheUpdate($id_noticia, $noticia, $titulo, $user_id, $hora);

		}


	}