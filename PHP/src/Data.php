<?php 


	namespace App;


	/**
	* Funciones de notificacion.
	*
	*/
	Class Data
	{
		use XSS;

		/**
		* Chequea que la data exista.
		*
		* @return bool
		*/
		static function checkData()
		{
			if(isset($_POST['data']))
				return 1;

			return 0;
		}



		/**
		* Notifica una noticia correctamente posteada.
		*
		* @return void
		*/
		static function postSuccess()
		{
			if(isset($_GET['posting'])): 

            	if($_GET['posting'] == 'true') : 

            		?>
		            <div class="alert alert-success alert-dismissible" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		              	<strong>Su post ha sido publicado!</strong>.
		            </div>
         	 		<?php 
         	 	endif; 
        	
        	endif; 
		}



		/**
		* Notifica error de token
		*
		* @return void
		*/
		static function errorCSRF()
		{
			if(isset($_GET['csrf'])): 

            	if($_GET['csrf'] == 'false') : 

            		?>
		            <div class="alert alert-danger alert-dismissible" role="alert">
		              	<strong>CSRF token no corresponde, cierre otras instancias abiertas y reintente!</strong>.
		            </div>
         	 		<?php 
         	 	endif; 
        	
        	endif; 
		}



		/**
		* Notifica error de autentificacion
		*
		* @return void
		*/
		static function errorAuth()
		{
			if(isset($_GET['auth'])): 

            	if($_GET['auth'] == 'false') : 

            		?>
		            <div class="alert alert-danger alert-dismissible" role="alert">
		              <strong>Error de autentificacion</strong>.
		            </div>
         	 		<?php 
         	 	endif; 
        	
        	endif; 
		}

		


		/**
		* Notifica error de cookie
		*
		* @return void
		*/
		static function errorNotCookie()
		{
			if(isset($_GET['cookie'])): 

            	if($_GET['cookie'] == 'false') : 

            		?>
		            <div class="alert alert-danger alert-dismissible" role="alert">
		              <strong>Login Fail due cookie</strong>.
		            </div>
         	 		<?php 
         	 	endif; 
        	
        	endif; 
		}


		/**
		* Notifica sesion iniciada
		*
		* @return void
		*/
		static function sessionSuccess()
		{
			
    		?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <strong>Session on!</strong>.
            </div>
 	 		<?php 
         	 	
		}



		/**
		* Compilacion de errores a mostrar.
		*
		* @return void
		*/
		static function errors()
		{	
			self::errorCSRF();
			self::errorNotCookie();
			self::errorAuth();
			self::postSuccess();

		}
	}

 ?>