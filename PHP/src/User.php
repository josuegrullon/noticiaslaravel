<?php 

	namespace App;

	use App\ConectDatabase;



	/**
	 * @class User
	 * 
	 * @brief Operaciones de usuario en la base de datos.
	 * 
	 */
	class User 
	{


		use UserSession;

		/**
		* Encuentra un usuario validandolo con su contrase;a.
		*
		* @param string $user
		* @param string $pass
		*
		* @return bool
		*/
		public function find($user, $pass)
		{

		   
			$stmt =  ConectDatabase::getInstance()->prepare("SELECT * FROM users Where `user`='$user' AND `password`='$pass'");
			
			$stmt->execute();
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);

			if( ! $row)
				return 0;

			return 1;
					   
		}




		/**
		* Encuentra usuario por ID.
		*
		* @param string $user
		* @param string $id
		*
		* @return bool
		*/
		public function matchUser($user, $id)
		{

		   
			$stmt =  ConectDatabase::getInstance()->prepare("SELECT * FROM users Where `user`='$user' AND `id`='$id'");
			
			$stmt->execute();
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);

			if( ! $row)
				return 0;

			return 1;
					   
		}




		/**
		* Obtiene el ID del usuario
		*
		* @param string $user
		* @param string $pass
		*
		* @return int
		*/
		public function getId($user, $pass)
		{

			$stmt = ConectDatabase::getInstance()->prepare("SELECT * FROM users Where `user`='$user' AND `password`='$pass'");
			$stmt->execute();
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);

			return $row['id'];
					   
		}



		/**
		* Obtiene usuario
		*
		* @param string $user
		* @param string $pass
		*
		* @return string
		*/
		public function getUser($user, $pass)
		{

			$stmt =  ConectDatabase::getInstance()->prepare("SELECT * FROM users Where `user`='$user' AND `password`='$pass'");
			$stmt->execute();
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);

			return $row['user'];
					   
		}




		/**
		* Obtiene nombre completo
		*
		* @param string $user
		* @param string $pass
		*
		* @return string
		*/
		public function getFullName($user, $pass)
		{

			$stmt =  ConectDatabase::getInstance()->prepare("SELECT * FROM users Where `user`='$user' AND `password`='$pass'");
			$stmt->execute();
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);

			return $row['nombres']." ".$row['apellidos'];
					   
		}



	}

 ?>