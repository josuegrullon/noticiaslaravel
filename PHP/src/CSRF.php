<?php

    namespace App;

		

    /**
    * @class CSRF.
    * @brief Proteccion CSRF
    */
    class CSRF
    {	
        /**
        * Variable de hash random para token.
        *
        * @var string $hash
        */
    	public  $hash;


        /**
        * Variable de hash random para token previa.
        *
        * @var string $previousHash
        */
 		public  $previousHash;



        /**
        * Constructor de la clase.
        *
        * @return void
        */
     	public function __construct()
     	{
    		if(isset($_SESSION['request_token']))
    			$this->previousHash = $_SESSION['request_token'];
    				
            $this->hash = $_SESSION['request_token'] = md5(uniqid());// inicializo el token del form
       
     	}


        /**
        * Chequea que el token este seteado.
        *
        * @return bool
        */
     	public static function checkPost()
     	{
     	    if(isset($_POST['csrf_token']))
     	    	return true;
     	    else
     	    	return false;
     	
     	}



        /**
        * Valida el token de un formulario.
        *
        * @return bool
        */
        public function isValid()
     	{
     	    if( $this->checkPost() && $_POST['csrf_token'] === $this->previousHash){           
                return 1;
            }
     	    else{
                 
     	    	return 0;
            }
     	
     	}



        /**
        * Redirecciona en caso de que no sea valido el token.
        *
        * @return void
        */
        public function redirectCSRF()
        {
            if(!$this->isValid()){

                if(basename($_SERVER['PHP_SELF']) != "Login.php"){  

                        if(Dir::getParentDir(getcwd()) == Dir::obtainDir())   
                            header('location:pages/Login.php?csrf=false');
                        else
                            header('location:Login.php?csrf=false');
                    }

            }


                
        }
      

    }
