<?php 

	namespace App;
	
	/**
	* Funciones de directorios.
	*
	*/
	class Dir 
	{	

		
		/**
		* Consige el directorio padre o antecesor. 
		*
		* @param string $path
		* @param int    $index
		*
		* @return string Nombre de directorio actual o antecesor.
		*/
		static function getParentDir($path /*a usar con getcwd()*/, $index = null)
		{
			//detecta sistema operativo para usar 
			$os = ((strpos(strtolower(PHP_OS), 'win') === 0) ||
			(strpos(strtolower(PHP_OS), 'cygwin') !== false)) ? 'win32' : 'unix';
			
			$slash = '/';

			if($os == 'win32')
			$slash = '\\';
			$array = explode($slash, $path);

			if($index  != null)
			$actual = $index;
			else
			$actual = count($array) - 1;

			return $array[$actual];
		}


		/**
		* Consige el directorio root. (shortcut)
		*
		* @return string Nombre de directorio root.
		*/
		public static function obtainDir()
		{

			return  Dir::getParentDir(getcwd(), 6); 
		}

	}
