<?php 

	namespace App;

	require_once '../config/connect.php';
	
    /**
    * @class ConectCache
    * @brief Gestiona una instancia para la conexion al servidor de cache
    */
    class ConectCache implements Singleton {
       
        /**
        * Gestor de instancia
        *
        * @var  $instance
        */
        private static $instance;


        /**
        * Variable de conexion a cache
        *
        * @var $cache
        */
        private $cache;



        /**
        * Constructor de la clase, inicializa Memcached y conecta con servidor
        *
        * @return void
        */
        public function __construct() {
 
                $this->cache = new \Memcached;
				$this->cache->addServer(CACHE_SERVER, CACHE_PORT);
        }

        /**
        * Devuelve una instancia de la conexion a memcached.
        *
        * @return Memcached
        */
        public static function getInstance() {

            if(self::$instance === null) {

              self::$instance = new ConectCache();
            }

            return self::$instance->cache;
        }
    }



 ?>