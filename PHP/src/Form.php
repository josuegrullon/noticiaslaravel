<?php
	
	namespace App;
	
	use App\ConectDatabase;
	use App\Cache;


	/**
	* Grupo de formularios.
	*
	*/
	 class Form
	{
		

		/**
		* Muestra la tabla de noticas del usuario en memcache o en base de datos.
		*
		* @return void
		*/
		static function showNoticeTable()
		{
			if(isset($_SESSION['user_id'])){
				$mem = new Cache();
				echo "
				<table class='table'>
					<thead>
						<tr>
							<th>Titulo </th>
							<th>Noticia</th>
							<th>Accion </th>
						</tr>
					</thead> 
				<tbody>";

				$user_id = $_SESSION['user_id'];
				$usuariosId = $mem->userKeys();
				$noticiasId = $mem->noticiaId();

				$ln = count($usuariosId);
				$row = [];

				if ($ln > 0) {// si hay algo cargado en memcache tiralo primero
				

					for ($i=0; $i < $ln ; $i++) { 

						if($user_id == $usuariosId[$i]){

							$row = ($mem->cacheGet($noticiasId[$i], $usuariosId[$i]));
							echo "
							<tr>
								<td>".$row['titulo']."</td>
								<td>".$row['noticia']."</td>
								<td>
									<i>
										<a href='Home.php?ver=true&id=".$row['id']."'>   Ver   </a>   |
										<a href='Home.php?edit=true&id=".$row['id']."'>  Editar</a>   |
										<a href='Home.php?delete=true&id=".$row['id']."'>Borrar</a> 
									</i>
								</td>
							</tr>";

						}

					}
				} else {	

					$bd = ConectDatabase::getInstance();

					$idUser = $_SESSION['user_id'];

					$sql= "SELECT * FROM noticias WHERE user_id='$idUser'";

					foreach ($bd->query($sql) as $row) {

						echo "
						<tr>
							<td>".$row['titulo']."</td>
							<td>".$row['noticia']."</td>
							<td>
								<i>
									<a href='Home.php?ver=true&id=".$row['id']."'>Ver</a>|
									<a href='Home.php?edit=true&id=".$row['id']."'>Editar</a>|
									<a href='Home.php?delete=true&id=".$row['id']."'>Borrar</a> 
								</i>
							</td>
						</tr>";


					}


				}


				echo "
					</tbody>
				</table>";

			} else {
				
				throw new Exception("Error Processing Request not session id user set");

			}

		}
		

		/**
		* Formulario de insercion.
		*
		* @return void
		*/
		static function insertForm()
		{
			echo "<label>Inserting </label>
				<form method='POST' action='central.php'  class='form-group'>
							
							<div class='form-group'>
							  	
								<input  type='hidden'   name='csrf_token'  value='".(new CSRF)->hash."' >

								<label>Titulo: </label> 
								<input  type='text'      
								class='form-control' name='form[titulo]'  placeholder='Agregar titulo' 
								autocomplete='off' required> 

								<label>Noticia:</label>
								<textarea  type='textarea'  
								class='form-control' name='form[noticia]' placeholder='Agregar acontecimiento' 
								autocomplete='off' rows='4' required></textarea>

								<button type='submit' class='btn btn-info ' >Post</button>
							
							</div>
						</form>";

		}


		/**
		* Formulario de edicion de noticia.
		*
		* @return void 
		*/
		static function editForm()
		{
			if(isset($_GET['edit']) && isset($_GET['id'])) {
				
				$bd = ConectDatabase::getInstance();
				
				$id     = $_GET['id'];
				$idUser = $_SESSION['user_id'];
				$sql    = "SELECT * FROM noticias WHERE user_id='$idUser' AND id='$id'";
				$stmt   = $bd->query($sql);
				$result = $stmt->fetch(\PDO::FETCH_ASSOC);

				echo "<label>Updating </label>
					<form method='POST' action='central.php'  class='form-group'>
								
								<div class='form-group'>
								  	
									<input  type='hidden'   name='csrf_token'  value='".(new CSRF)->hash."' >
									<input  type='hidden'   name='update'      value='true' >
									<input  type='hidden'   name='id_noticia'  value='".$result['id']."' >
									<label>Titulo: </label> 
									<input  type='text'      
									class='form-control' name='form[titulo]'  placeholder='Agregar titulo' 
									autocomplete='off' value='".$result['titulo']."' required> 

									<label>Noticia:</label>
									<textarea  type='textarea'  
									class='form-control' name='form[noticia]' placeholder='Agregar acontecimiento' 
									autocomplete='off' rows='4' required>".$result['noticia']."</textarea>

									<button type='submit' class='btn btn-info ' >EDIT</button>
								
								</div>
							</form>";
			}	else {

					return $this->insertForm();
					
			}


		}




		/**
		* Formulario para borrar noticia
		*
		* @return void
		*/
		static function deleteForm()
		{
			if(isset($_GET['delete']) && isset($_GET['id'])) {
				
				$bd = ConectDatabase::getInstance();

				$id     = $_GET['id'];
				$idUser = $_SESSION['user_id'];
				$sql    = "SELECT * FROM noticias WHERE id='$id'";
				$stmt   = $bd->query($sql);
				$result = $stmt->fetch(\PDO::FETCH_ASSOC);

				echo "<label>Deleting </label>
					<form method='POST' action='central.php'  class='form-group'>
								
								<div class='form-group'>
								  	
									<input  type='hidden'   name='csrf_token'     value='".(new CSRF)->hash."' >
									<input  type='hidden'   name='delete'         value='true' >
									<input  type='hidden'   name='form[noticia]'  value='true' >
									<input  type='hidden'   name='form[titulo]'   value='true' >
									<input  type='hidden'   name='id_noticia'     value='".$result['id']."' >
									
									Realmente quiere eliminar este post? ".$result['titulo']."

									
									<button type='submit' class='btn btn-info ' >Borrar</button>
								
								</div>
							</form>";
			}	else {

					return $this->insertForm();
					
			}


		}
		



		/**
		* Formulario para ver noticia.
		*
		* @return void
		*/
		static function viewForm()
		{
			if(isset($_GET['ver']) && isset($_GET['id'])) {
				
				$bd = ConectDatabase::getInstance();

				$id     = $_GET['id'];
				$idUser = $_SESSION['user_id'];
				$sql    = "SELECT * FROM noticias WHERE user_id='$idUser' AND id='$id'";
				$stmt   = $bd->query($sql);
				$result = $stmt->fetch(\PDO::FETCH_ASSOC);

				echo "<label>Viewing </label>
					<form class='form-group'>
								
								<div class='form-group'>
								  	
									<input  type='hidden'   name='csrf_token'  value='".(new CSRF)->hash."' >
									<input  type='hidden'   name='update'      value='true' >
									<input  type='hidden'   name='id_noticia'  value='".$result['id']."' >
									<label>Titulo: </label> 
									<input  type='text'      
									class='form-control' name='form[titulo]'  placeholder='Agregar titulo' 
									autocomplete='off' value='".$result['titulo']."' required> 

									<label>Noticia:</label>
									<textarea  type='textarea'  
									class='form-control' name='form[noticia]' placeholder='Agregar acontecimiento' 
									autocomplete='off' rows='4' required>".$result['noticia']."</textarea>

									
								
								</div>
							</form>";
			}	else {

					return $this->insertForm();
					
			}

		}




		/**
		* Gestor de formulario.
		*
		* @return void, formulario
		*/
		static function formulario()
		{
			if(isset($_GET['edit'])) {

				return self::editForm();
			
			} else if(isset($_GET['ver'])) {
			
				return self::viewForm();

			} else if (isset($_GET['delete'])){

				return self::deleteForm();

			} else {

				return self::insertForm();
			}

		}




	}