<?php 


    namespace App;

    require_once '../config/connect.php';



    /**
     * @class ConectDatabase
     * @brief Gestiona la conexion a la base de datos.
     * 
     */
    class ConectDatabase implements Singleton {
       

       /**
        * Gestor de instancia
        *
        * @var  $instance
        */
        private static $instance;


         /**
        * Variable de conexion a PDO
        *
        * @var $pdo
        */
        private $pdo;


        /**
        * Constructor de la clase, inicializa PDO y conecta con credenciales
        *
        * @return void
        */
        public function __construct() {

                try {

                    $this->pdo = new \PDO("mysql:host=".BD_HOST_NAME.";dbname=".BD_BASE_DATOS."",BD_USER_NAME, BD_PASSWORD);
                        
                } catch (\Exception $e) {

                    echo $e->getMessage();
                }
        }



         /**
        * Devuelve una instancia de la conexion a PDO.
        *
        * @return PDO
        */
        public static function getInstance() {

            if(self::$instance === null) {

              self::$instance = new ConectDatabase();
            }

            return self::$instance->pdo;
        }
    }

 //$db = Conect::getInstance();