<?php 

	require '../vendor/autoload.php';
	require '../header/header.php'; 

	use App\User;
	use App\CSRF; 
	use App\Data;
	use App\Noticia;
	use App\Form;

	User::validSessionPages();

	?>
	<br>
	<div class="container">

		<!-- Static navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
					data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Noticias PHP</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="Home.php">HOME</a></li>
					
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="#">
								<?=User::userName() ?>
							</a>
						</li>
						<li  class="btn-danger "> 
							<a style="color:white;" href='<?=User::closeUrl() ?>'>
								<b>Log out </b>
							</a>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div><!--/.container-fluid -->
		</nav>

		<!-- Main component for a primary marketing message or call to action -->
		
		<?php Data::errors(); ?>
		<div class="jumbotron">
			<div class="row">
				<div class="col-lg-6">
					<div class="input-group">
						<!-- para probar -->
						<?php Form::formulario() ?>
					</div>
				</div>
			</div>


		
		</div> <!-- /jumbotron -->
		
		<div class="jumbotron">

			<?php Form::showNoticeTable();  ?>
		</div>
	</div> <!-- /container -->





	<?php require '../footer/footer.php'; ?>