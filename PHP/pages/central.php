<?php 

	require '../vendor/autoload.php';
	require '../header/header.php'; 
	

	use App\CSRF;
	use App\Data; 
	use App\Noticia; 
	use App\User;

	User::validSessionPages();

	if((new CSRF)->isValid()){

		if(isset($_POST['form'])){
			$dataUser = Data::htmlArrayPurifier($_POST['form']);
		}
		else{
			throw new Exception("user data VACIO");
		}

		$user_id = $_SESSION['user_id'];
		$hora    = (new \DateTime())->format('Y-m-d H:i:s');
		$noticia = $dataUser['noticia'];
		$titulo  = $dataUser['titulo'];

	
			
		
		if(isset($_POST['update'])) {

			$id_noticia = $_POST['id_noticia'];
			(new Noticia)->updateNoticia($noticia, $titulo, $id_noticia,$hora);

		} else if(isset($_POST['delete'])) {
			$id_noticia = $_POST['id_noticia'];
			(new Noticia)->deleteNoticia($id_noticia);
		
		} else {

			(new Noticia)->insertNoticia($noticia, $titulo, $user_id, $hora);
		}

		
		header('location:Home.php?posting=true');
	} else {
	  
	    header('location:Home.php?csrf=false');
	               

	}







?>