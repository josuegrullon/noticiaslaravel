<?php 
	require '../vendor/autoload.php';

    session_start();

 
    use App\CSRF;
    use App\Data;
    use App\User;
 	use App\Dir;
 	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookSDKException;
	use Facebook\GraphUser;
	use Facebook\FacebookResponse;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use App\FBSession;
	


	FacebookSession::setDefaultApplication(FBSession::$id, FBSession::$secret);

	$helper = new FacebookRedirectLoginHelper('http://10.0.0.101/josue/Noticias/PHP/pages/link.php');

	
	try {
		$sessionFB= $helper->getSessionFromRedirect();

	} catch(FacebookRequestException $ex) {
		   
	} catch(\Exception $ex) {
		  
	}


	if(!isset($sessionFB)) {

		if(isset($_POST['data']))
		    	$dataUser = Data::htmlArrayPurifier($_POST['data']);
		   	else
		   		throw new Exception("user data VACIO");

	    if((new CSRF)->isValid()){
	    	
	    	if(User::checkCookie()){
	    		echo "la cookie existe<br>";
	    		
	    		if((new User)->matchUser($_COOKIE['local_data']['user_name'], $_COOKIE['local_data']['user_id'] )) {

		    		$_SESSION['user_id']       = $_COOKIE['local_data']['user_id'];
					$_SESSION['user_name']     = $_COOKIE['local_data']['user_name'];
					$_SESSION['user_fullName'] = $_COOKIE['local_data']['user_fullName'];

		    		header('location:Home.php');

		    	} else {
		    		
		    		throw new Exception("No match cookie with user");
		    		
		    	}

	    	} else {
	    		
	    		if((new User)->find($dataUser['user'], $dataUser['password'])) {

	    			$_SESSION['user_id']       = (new User)->getId(  $dataUser['user'],     $dataUser['password']);
					$_SESSION['user_name']     = (new User)->getUser($dataUser['user'],     $dataUser['password']);
					$_SESSION['user_fullName'] = (new User)->getFullName($dataUser['user'], $dataUser['password']);

	        		if(!isset($_COOKIE['local_data']) && $dataUser['save'] == "on"){
	        			
	        			$userid = (new User)->getId(  $dataUser['user'], $dataUser['password']);

	        			

	        			setcookie("local_data[user_id]"  , $userid);
						setcookie("local_data[user_name]", $dataUser['user']);
						setcookie("local_data[user_fullName]", (new User)->getFullName($dataUser['user'], $dataUser['password']));
	        		}

	        		header('location:Home.php');
	    		}
	    		else{
	    		    header('location:Login.php?auth=false');
	    		}



	    	}
		   	
	    } else {


	        if(basename($_SERVER['PHP_SELF']) != "Login.php"){  

	                if(Dir::getParentDir(getcwd()) == Dir::obtainDir())   
	                    header('location:pages/Login.php?csrf=false');
	                else
	                    header('location:Login.php?csrf=false');
	            }

	            
	    }


	} else {

		try {

	    	$user_profile = (new Facebook\FacebookRequest(
	        $sessionFB, 'GET', '/me'
	   		))->execute()->getGraphObject(GraphUser::className());
		   
		    #setting session vars
		   	$_SESSION['user_id']       =  $user_profile->getId();
			
		   	$_SESSION['user_name']     =  $user_profile->getName();

		    $_SESSION['user_fullName'] =  $user_profile->getName();

		   	header('location:Home.php');

		} catch(FacebookRequestException $e) {

		    echo "Exception occured, code: " . $e->getCode();
		    echo " with message: " . $e->getMessage();

		}   

	}




 ?>