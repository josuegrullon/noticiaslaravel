
    <!-- Autoload -->
    <?php require '../vendor/autoload.php'; ?>
    <!-- Header -->
    <?php require '../header/header.php'; ?>

    <?php session_start(); ?>
    
    <?php 

    use App\FBSession;
    use App\CSRF; 
    use App\User;
    use App\Data;

    User::validSession();
     ?>

    <!-- Wrap all page content here -->
    <div id="wrap">

      <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="#">Noticias PHP</a>
                </div>

            </div>
        </div>

        <!-- Begin page content -->
        <div class="container">

            <div class="page-header">
                <strong><h3>Log in</h3></strong>
            </div>

            <div class="col-sm-6 col-md-4">



             <?php Data::errors(); ?>



                <a href='<?=FBSession::fbLogin(); ?>'>
                    <button class="btn btn-lg btn-primary btn-block" type="button">
                        Log in with Faceebook
                    </button>
                </a>
                <br>
                <form class="form-signin " method="POST" action="link.php" >

                    <input type='hidden' name="csrf_token"  value="<?php echo (new CSRF)->hash ?>" >
                  
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="User" 
                        name="data[user]" autocomplete="off" required/>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" 
                        autocomplete="off" name="data[password]" required />
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="data[save]" value="off"> 
                        <input type="checkbox" name="data[save]">     
                        <label>Remember me</label>
                    </div>

                  
                    <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
                </form>



            </div>
        </div>
    </div>


    <!-- Footer -->
    <?php require '../footer/footer.php'; ?>



