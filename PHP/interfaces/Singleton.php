<?php 

	namespace App;

	interface Singleton {

        public static function getInstance();
    }

 ?>