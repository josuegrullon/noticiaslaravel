<?php 

	namespace App;

	use App\Dir;



	/**
	 * Administra la sesion del usuario.
	 */
	trait UserSession
	{
		



		/**
		* Chequea la cookie
		*
		* @return bool
		*/
	    static function checkCookie(){

			if(!isset($_COOKIE['local_data']['user_id']))
				return 0;

			return 1;
		}



		/**
		* Chequea la sesion
		*
		* @return bool
		*/
		static function checkSession()
		{
			if(!isset($_SESSION['user_id']))
				return 0;

			return 1;

		}

		
		/**
		* Valida la sesion
		*
		* @return void
		*/
		static function validSession()
		{
			if(!self::checkSession()){
				
				
				if(!self::checkCookie()){

					#go to login. Para evitar el bucle de redireccionamiento:
					if(basename($_SERVER['PHP_SELF']) != "Login.php"){  

						if(Dir::getParentDir(getcwd()) == Dir::obtainDir() )	
							header('location:pages/Login.php?cookie=false');
					    else
							header('location:Login.php?cookie=false');
					}

				} else {

					$_SESSION['user_id']       = $_COOKIE['local_data']['user_id'];
					$_SESSION['user_name']     = $_COOKIE['local_data']['user_name'];
					$_SESSION['user_fullName'] = $_COOKIE['local_data']['user_fullName'];
					
				}


			} else {

				#go to home. Para evitar el bucle de redireccionamiento:
				if(basename($_SERVER['PHP_SELF']) != "Home.php"){  

					if(Dir::getParentDir(getcwd()) == Dir::obtainDir() )	
						header('location:pages/Home.php');
				    else
						header('location:Home.php');
				}
			}
		}





		/**
		* Valida la sesion entre paginas
		*
		* @return void
		*/
		static function validSessionPages()
		{
			session_start();
			if(!self::checkSession()){
				
				if(!self::checkCookie()){

					#go to home. Para evitar el bucle de redireccionamiento:
					if(basename($_SERVER['PHP_SELF']) != "Login.php"){  

						if(Dir::getParentDir(getcwd()) == Dir::obtainDir())	
							header('location:pages/Login.php');
					    else
							header('location:Login.php');
					}
					
				} else {
					$_SESSION['user_id']       = $_COOKIE['local_data']['user_id'];
					$_SESSION['user_name']     = $_COOKIE['local_data']['user_name'];
					$_SESSION['user_fullName'] = $_COOKIE['local_data']['user_fullName'];
				}
				


			}
		}




		/**
		* Destruye la session
		*
		* @return void
		*/
		public static function closeSession()
		{
			
			session_start();

			session_destroy();
		    
		    setcookie('local_data[user_id]', '', time()-1000);
		    setcookie('local_data[user_name]', '', time()-1000);
		    setcookie('local_data[user_fullName]', '', time()-1000);

		 
            if(Dir::getParentDir(getcwd()) == Dir::obtainDir() )   
                header('location:pages/Login.php');
            else
                header('location:Login.php');
	            
		}

		public static function closeUrl()
		{

			return "destroySession.php";
		}
		

		public static function userName()
		{

			if(isset($_SESSION['user_fullName']))
				return $_SESSION['user_fullName'];

		//throw new \Exception("Error al cargar el nombre en var session");
			
		}
	}

 ?>