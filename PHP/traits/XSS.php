<?php  
	
	namespace App;

    /**
     * Sanitizacion del input del usuario.
     */
	trait XSS 
	{
		
		
        /**
        * Recibe array del usuario y lo limpia.
        *
        * @param array $html
        *
        * @return array
        */
    	public static function htmlArrayPurifier($html)
    	{
    		
    		$purifier = new \HTMLPurifier(\HTMLPurifier_Config::createDefault());
    		

    		$arrayConejo = array();

    		foreach ($html as $key => $value) {
    			
    			$arrayConejo[$key] = $purifier->purify($value);

    		}


    		return $arrayConejo;
    	}




        /**
        * Recibe input del usuario y lo limpia.
        *
        * @param string $html
        *
        * @return string
        */
        public static function htmlPurifier($html)
        {
            
            $purifier = new \HTMLPurifier(\HTMLPurifier_Config::createDefault());
    
            return $purifier->purify($html);
        }
	}
 ?>