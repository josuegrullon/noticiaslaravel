<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
     <style type="text/css">

         body
        {
            zoom: 85%;
        }
     </style>
    <!-- Bootstrap core CSS -->
    <link href="../vendor/twitter/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer-navbar.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">

        setTimeout(function() {
            $('.alert').fadeOut('slow');
        }, 2000); // <-- time in milliseconds

    </script>

  </head>
    <body>